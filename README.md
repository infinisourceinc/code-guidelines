# Code Guidelines

**All files should end with an extra trailing carriage return.**
> This is intended to make human-generated files match the convention of machine-generated files. This way any machine-generated file should match our conventions so it's more of a seamless union.

**Indentation should be done with spaces, not tabs**
> Additionally, all files should have a tab-width of 4 spaces. This can easily be configured in any code editor.

**Control flow statements should have a space between the signature elements**
> Each element of a control flow statement signature should have spaces between them, with the single exception of commas. A comma doesn't need a space between the character preceding it.  
> For example: `if (arg1, arg2, arg3) { ... }`  
> instead of: `if(arg1,arg2,arg3){...}`  
> This makes the code more readable at a glance. Remember that leading and kerning are just important in code as they are in published text. Since you're staring at a screen all day, make it as easy on your eyes as you can.

**All class methods should have a docblock comment**
> When writing scripted languages in an object-oriented approach, all methods within an object should have a docblock-style comment above them to explain their purpose. Additionally, use these comments to explain the function's arguments and return values. If using a namespaced language, be sure to include full namespaces where appropriate. Comment parameters should have two spaces after `@param` as opposed to the one space afforded to `@return`. This makes the `@param` and `@return` lines line up. Try to `@param` names with each other if they're close enough to improve readability.  
> 
> *Example:*  
![alt text](https://i.imgur.com/qaegjju.png "Docblock Example")

**Placement of curly braces depends on context**
> - Next line  
>> - Class methods  
>> - Named functions  
> - Same Line  
>> - Anonymous functions  
>> - Arrow functions  
>> - Control flow statements  

**HTML Indentation**
> - Every descendant gets another indent  
> - Siblings should have a carriage return between them  
> - The `<head>` and `<body>` tags themselves do not need to be indented  
> - Any time that you are going to write a comment, ask yourself if the same thing can be explained with a class  
> - If an element's classes already describe its purpose or appearance, you don't need a comment to explain it  
> 
> Exceptions may be made in the event that indentation causes a display issue (e.g. Having inline siblings on different lines adding margin).
